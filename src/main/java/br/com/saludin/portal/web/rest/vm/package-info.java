/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.saludin.portal.web.rest.vm;
