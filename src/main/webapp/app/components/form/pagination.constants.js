(function() {
    'use strict';

    angular
        .module('saludinApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
